# TP3 : Progressons vers le réseau d'infrastructure

# I. (mini)Architecture réseau

## 1. Adressage


## 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

    [david@router ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
           valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:ad:4d:25 brd ff:ff:ff:ff:ff:ff
        inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
           valid_lft 86161sec preferred_lft 86161sec
        inet6 fe80::a00:27ff:fead:4d25/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:8e:60:3b brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8
           valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe8e:603b/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:7c:d1:ee brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s9
           valid_lft forever preferred_lft forever
        inet6 fe80::3b7b:d4ee:f2b0:6367/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:f2:66:84 brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
           valid_lft forever preferred_lft forever
        inet6 fe80::3652:3e2c:c2e6:19c7/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    
il a un accès internet

    [david@router ~]$ ping 8.8.8.8 -c 1
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.7 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.708/19.708/19.708/0.000 ms

il a de la résolution de noms

    [david@router ~]$ ping google.com -c 1
    PING google.com (142.250.179.110) 56(84) bytes of data.
    64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=114 time=38.5 ms

    --- google.com ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 38.479/38.479/38.479/0.000 ms

il porte le nom `router.tp3`*    

    [david@router ~]$ cat /etc/hostname
    router.tp3
    
n'oubliez pas [d'activer le routage sur la machine](../../cours/memo/rocky_network.md#activation-du-routage)

    [david@router ~]$ cat /etc/sysctl.d/routeur.conf
    net.ipv4.ip_forward=1
    
    [david@router ~]$ sudo firewall-cmd --info-zone=public
    [sudo] password for david:
    public (active)
      [...]
      masquerade: yes
      [...]


# II. Services d'infra

## 1. Serveur DHCP

🌞 **Mettre en place une machine qui fera office de serveur DHCP** dans le réseau `client1`. Elle devra :

 porter le nom `dhcp.client1.tp3`
 
     [david@dhcp ~]$ cat /etc/hostname
    dhcp.client1.tp3

donner une IP aux machines clients qui le demande


    [root@dhcp ~]# cat /etc/dhcp/dhcpd.conf
    [...]    
    subnet 10.3.1.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.130 10.3.1.188;
    [...]

                               }

leur donner l'adresse de leur passerelle
    
    [root@dhcp ~]# cat /etc/dhcp/dhcpd.conf
    [...]
    # specify gateway
    option routers     10.3.1.190;
    [...]
    
leur donner l'adresse d'un DNS utilisable
        
    [root@dhcp ~]# cat /etc/dhcp/dhcpd.conf
    [...]
    option domain-name-servers  1.1.1.1   ;
    [...]

📁 **Fichier `dhcpd.conf`**

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/Configurations/dhcp_conf.conf


🌞 **Mettre en place un client dans le réseau `client1`**

de son p'tit nom `marcel.client1.tp3`
    
    [david@bastion-ovh1fr ~]$ cat /etc/hostname
    marcel.client1.tp3


la machine récupérera une IP dynamiquement grâce au serveur DHCP

    [david@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    BOOTPROTO=dhcp
    [...]
    NAME=enp0s8
    [...]

ainsi que sa passerelle et une adresse d'un DNS utilisable

    [david@marcel ~]$ ip r s
    [...]
    default via 10.3.1.190 dev enp0s8 proto dhcp metric 101
    [...]
    
    [david@marcel ~]$ cat /etc/resolv.conf
    ; generated by /usr/sbin/dhclient-script
    search client1.tp3
    nameserver 1.1.1.1

🌞 **Depuis `marcel.client1.tp3`**

prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

    [david@marcel ~]$ ping 8.8.8.8 -c1
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=20.3 ms

    --- 8.8.8.8 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 20.276/20.276/20.276/0.000 ms
    
    [david@marcel ~]$ dig google.com
    [...]
    ;; ANSWER SECTION:
    google.com.             144     IN      A       172.217.19.238

    [...]
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    [...]

à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau

    [david@marcel ~]$ sudo traceroute 8.8.8.8
    [sudo] password for david:
    traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
     1  _gateway (10.3.1.190)  0.980 ms  1.000 ms  1.003 ms  <--- IP du routeur
    [...]

## 2. Serveur DNS

### SETUP copain


🌞 **Mettre en place une machine qui fera office de serveur DNS**

de son p'tit nom `dns1.server1.tp3`
    
    [david@bastion-ovh1fr ~]$ cat /etc/hostname
    dns1.server1.tp3


il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme `google.com`

    [david@dns1 ~]$ cat /etc/resolv.conf
    # Generated by NetworkManager
    search server1.tp3

    nameserver 1.1.1.1


🌞 **Tester le DNS depuis `marcel.client1.tp3`**

définissez **manuellement** l'utilisation de votre serveur DNS

    [david@marcel ~]$ sudo vim /etc/resolv.conf
    # Generated by NetworkManager
    search server1.tp3
    nameserver 10.3.1.125

essayez une résolution de nom avec `dig`
une résolution de nom classique

    [david@marcel ~]$ dig dhcp.client1.tp3

    [...]
    ;; ANSWER SECTION:
    dhcp.client1.tp3.       86400   IN      A       10.3.1.189
    [...]
    ;; Query time: 1 msec
    ;; SERVER: 10.3.1.125#53(10.3.1.125)
    [...]
    
    [david@marcel ~]$ dig dns1.server1.tp3

    [...]
    ;; ANSWER SECTION:
    dns1.server1.tp3.       86400   IN      A       10.3.1.125
    [...]
    ;; Query time: 1 msec
    ;; SERVER: 10.3.1.125#53(10.3.1.125)
    [...]



🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

les serveurs, on le fait à la main
    
    [david@dhcp ~]$ sudo vim /etc/resolv.conf
    search server1.tp3
    nameserver 10.3.1.125    


les clients, c'est fait *via* DHCP
    
    [root@dhcp ~]# vim /etc/dhcp/dhcpd.conf
     option domain-name-servers 10.3.1.125 ;


## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

faites en sorte que votre DNS soit désormais aussi un forwarder DNS

    [david@dns1 ~]$ sudo vim /etc/named.conf
    [sudo] password for david:
        [...]
            recursion yes;

        forwarders {
                8.8.8.8;
                1.1.1.1;
        };
        [...]


🌞 Test !

vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)

    [david@marcel ~]$ dig google.com

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 440
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 13, ADDITIONAL: 27

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ; COOKIE: 98405cc21868e0d29ae4962a61560b5e47eb0a0d8f96b57d (good)
    ;; QUESTION SECTION:
    ;google.com.                    IN      A

    ;; ANSWER SECTION:
    google.com.             78      IN      A       216.58.213.174

    ;; AUTHORITY SECTION:
    .                       518392  IN      NS      c.root-servers.net.
    .                       518392  IN      NS      a.root-servers.net.
    .                       518392  IN      NS      f.root-servers.net.
    .                       518392  IN      NS      e.root-servers.net.
    .                       518392  IN      NS      k.root-servers.net.
    .                       518392  IN      NS      g.root-servers.net.
    .                       518392  IN      NS      d.root-servers.net.
    .                       518392  IN      NS      h.root-servers.net.
    .                       518392  IN      NS      m.root-servers.net.
    .                       518392  IN      NS      b.root-servers.net.
    .                       518392  IN      NS      j.root-servers.net.
    .                       518392  IN      NS      i.root-servers.net.
    .                       518392  IN      NS      l.root-servers.net.

    ;; ADDITIONAL SECTION:
    m.root-servers.net.     518392  IN      A       202.12.27.33
    b.root-servers.net.     518392  IN      A       199.9.14.201
    c.root-servers.net.     518392  IN      A       192.33.4.12
    d.root-servers.net.     518392  IN      A       199.7.91.13
    e.root-servers.net.     518392  IN      A       192.203.230.10
    f.root-servers.net.     518392  IN      A       192.5.5.241
    g.root-servers.net.     518392  IN      A       192.112.36.4
    h.root-servers.net.     518392  IN      A       198.97.190.53
    i.root-servers.net.     518392  IN      A       192.36.148.17
    a.root-servers.net.     518392  IN      A       198.41.0.4
    j.root-servers.net.     518392  IN      A       192.58.128.30
    k.root-servers.net.     518392  IN      A       193.0.14.129
    l.root-servers.net.     518392  IN      A       199.7.83.42
    m.root-servers.net.     518392  IN      AAAA    2001:dc3::35
    b.root-servers.net.     518392  IN      AAAA    2001:500:200::b
    c.root-servers.net.     518392  IN      AAAA    2001:500:2::c
    d.root-servers.net.     518392  IN      AAAA    2001:500:2d::d
    e.root-servers.net.     518392  IN      AAAA    2001:500:a8::e
    f.root-servers.net.     518392  IN      AAAA    2001:500:2f::f
    g.root-servers.net.     518392  IN      AAAA    2001:500:12::d0d
    h.root-servers.net.     518392  IN      AAAA    2001:500:1::53
    i.root-servers.net.     518392  IN      AAAA    2001:7fe::53
    a.root-servers.net.     518392  IN      AAAA    2001:503:ba3e::2:30
    j.root-servers.net.     518392  IN      AAAA    2001:503:c27::2:30
    k.root-servers.net.     518392  IN      AAAA    2001:7fd::1
    l.root-servers.net.     518392  IN      AAAA    2001:500:9f::42

    ;; Query time: 109 msec
    ;; SERVER: 10.3.1.125#53(10.3.1.125)
    ;; WHEN: Thu Sep 30 21:09:18 CEST 2021
    ;; MSG SIZE  rcvd: 866



### B. On revient sur la conf du DHCP


🌞 **Affiner la configuration du DHCP**

faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
    
        [root@dhcp ~]# vim /etc/dhcp/dhcpd.conf
        [...]
       # specify DNS server's hostname or IP address
       option domain-name-servers 10.3.1.125 ;
       [...]

créer un nouveau client `johnny.client1.tp3` qui récupère son IP, et toutes les nouvelles infos, en DHCP

IP automatique: 

    [david@johnny ~]$ vim /etc/sysconfig/network-scripts/ifcfg-enp0s8
    [...]
    BOOTPROTO=dhcp
    [...]
    DEVICE=enp0s8
    ONBOOT=yes
    DNS1=10.3.1.125
    DOMAIN=server1.tp3

Routeur + internet:

    [david@johnny ~]$ ip r s
    default via 10.3.1.190 dev enp0s8 proto dhcp metric 100
    10.3.1.128/26 dev enp0s8 proto kernel scope link src 10.3.1.132 metric 100

    [david@johnny ~]$ ping -c1 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=27.0 ms

    --- 8.8.8.8 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 27.019/27.019/27.019/0.000 ms

DNS fonctionnel:

    [david@johnny ~]$ dig dns1.server1.tp3

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36708
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ; COOKIE: 9554c1bb030971015452ab7961560de57296a72be41f5a37 (good)
    ;; QUESTION SECTION:
    ;dns1.server1.tp3.              IN      A

    ;; ANSWER SECTION:
    dns1.server1.tp3.       86400   IN      A       10.3.1.125

    ;; AUTHORITY SECTION:
    server1.tp3.            86400   IN      NS      dns1.server1.tp3.

    ;; Query time: 1 msec
    ;; SERVER: 10.3.1.125#53(10.3.1.125)
    ;; WHEN: Thu Sep 30 21:20:05 CEST 2021
    ;; MSG SIZE  rcvd: 103


# III. Services métier

## 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**


Démarrage du service:

    [david@web1 ~]$ sudo systemctl start httpd.service
    [david@web1 ~]$ sudo systemctl enable httpd.service
    Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

Autorisation:

    [david@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [david@web1 ~]$ sudo firewall-cmd --reload
    success

Page html test:

    [david@web1 ~]$ sudo vim /var/www/html/index.html
    web1_test!!!

🌞 **Test test test et re-test**

testez que votre serveur web est accessible depuis `marcel.client1.tp3`

    [david@marcel ~]$ curl web1
    web1_test!!!



## 2. Partage de fichiers

### Le setup wola

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

**vous partagerez un dossier créé à cet effet : `/srv/nfs_share/`**

Installation de nfs:

    [david@nfs1 ~]$ sudo dnf install nfs-utils
    

Créatuon de l'export:

    [david@nfs1 ~]$ sudo mkdir /srv/nfs_share/
    [david@nfs1 ~]$ sudo vim /etc/exports
    /srv/nfs_share/         web1(rw)

Démarrage du service:

    [david@nfs1 ~]$ sudo systemctl start nfs-server.service
    [david@nfs1 ~]$ sudo systemctl enable nfs-server.service
    Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
  
Ouverture du port sur le firwall:

    [david@nfs1 ~]$ sudo firewall-cmd --add-service={nfs,nfs3,mountd,rpc-bind} --permanent
    success
    [david@nfs1 ~]$ sudo firewall-cmd --reload
    success
    
    [david@nfs1 ~]$ sudo exportfs -av
    exporting 10.3.1.205:/srv/nfs_share

🌞 **Configuration du client NFS**

Installation de nfs:

    [david@web1 ~]$ sudo dnf install nfs-utils
    [...]
    [david@web1 ~]$ sudo mkdir /srv/nfs

Définition du montage:

    [david@web1 nfs]$ sudo vim /etc/fstab
    [...]
    nfs1.server2.tp3:/srv/nfs_share         /srv/nfs               nfs     defaults        0 0
    
    [david@web1 nfs]$ sudo mount -a
    [david@web1 nfs]$
     
 Démarrage su service nfs:
 
    [david@web1 ~]$ sudo systemctl start nfs-server.service
    [david@web1 ~]$ sudo systemctl enable nfs-server.service
    Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
    
🌞 **TEEEEST**

tester que vous pouvez lire et écrire dans le dossier `/srv/nfs` depuis `web1.server2.tp3`

    [david@web1 nfs]$ echo "test Web1" > /srv/nfs/testons

vous devriez voir les modifications du côté de  `nfs1.server2.tp3` dans le dossier `/srv/nfs_share/`
        
    david@nfs1 ~]$ echo "j'ai bien lu !" >> /srv/nfs_share/testons
    [david@nfs1 ~]$ cat /srv/nfs_share/testons
    test Web1
    j'ai bien lu !

# IV. Un peu de théorie : TCP et UDP

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

SSH: 
    
    Port TCP 22 par défaut    

HTTP:

    Port TCP 80 par défaut
    
DNS:

    Port UDP 53 par défaut

NFS:

    Port TCP 2049 par défaut
    +Port UDP et TCP 111 par défaut


Capture http:

    [david@web1 ~]$ sudo tcpdump -i enp0s8 port 80 -w tp3_http.pcap
    [david@nfs1 ~]$ curl web1.server1.tp3
    web1_test!!!

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/TCP/UDP%20captures/tp3_http.pcap
    
Capture ssh:
    
    Depuis ma VM:
    
    [david@web1 ~]$ sudo tcpdump -i enp0s8 port 22 -w tp3_ssh.pcap
    
    Depuis mon hôte:
    
    PS C:\Users\rouss> ssh david@10.3.1.205

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/TCP/UDP%20captures/tp3_ssh.pcap
    
Capture DNS:
    
    [david@dns1 ~]$ sudo tcpdump -i enp0s8 port 53 -w tp3_dns.pcap
    david@web1 ~]$ dig nfs1.server2.tp3

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/TCP/UDP%20captures/tp3_dns.pcap

Capture nfs:

    [david@nfs1 ~]$ sudo tcpdump -i enp0s8 not port 22 -w tp3_nfs.pcap
    [david@web1 ~]$ cd /srv/nfs/

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/TCP/UDP%20captures/tp3_nfs.pcap

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

    Car les échanges de paquets ont lieu uniquement lors de la distribution d'IP

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/TCP/UDP%20captures/tp3_3ways.pcap

# V. El final

- 📁 Fichiers de zone

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/Configurations/server1.tp3.forward

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/Configurations/server2.tp3.forward

- 📁 Fichier de conf principal DNS `named.conf`

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/Configurations/named.conf

- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top

-le 🗃️ tableau des réseaux 🗃

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.128`        | `255.255.255.192` | 126                   | `10.3.1.190`         | `10.3.3.191`                                                                                   |
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 62                      | `10.3.1.126`         | `10.3.1.127`                                                                                   |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14                         | `10.3.1.206`         | `10.3.1.207` 


- le 🗃️ plan d'adressage IP

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.1.190/26`         | `10.3.1.126/25`         | `10.3.1.206/28`         | Carte NAT             |
| `dhcp.client1.tp3`| `10.3.1.189/26`        | ...                  | ...                  | `10.3.1.190/26`         |
| `marcel.client1.tp3`| `10.3.1.129/26`        | ...                  | ...                  | `10.3.1.190/26`       |
| `johnny.client1.tp3`| `10.3.1.130/26`        | ...                  | ...                  | `10.3.1.190/26`       |
| `dns1.server1.tp3`| ...                    | `10.3.1.125/25`      | ...                  | `10.3.1.126/25`         |
| `web1.server2.tp3`| ...                    | ...                  | `10.3.1.205/28`        | `10.3.1.206/28`       |
| `nfs1.server2.tp3`| ...                    | ...                  | `10.3.1.194/28`        | `10.3.1.206/28`       |


- le schéma

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP3/sch%C3%A9ma_tp3.drawio
