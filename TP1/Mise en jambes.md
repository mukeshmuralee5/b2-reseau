# TP1- Mise en jambes

#  I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### Affichez les infos des cartes réseau de votre PC


 #### Nom, adresse MAC et adresse IP de l'interface WiFi/
    
    PS C:\WINDOWS\system32> ipconfig /all
    
    Carte réseau sans fil Wi-Fi :

       [...]
       Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
       Adresse physique . . . . . . . . . . . : 58-96-1D-14-CA-7B
       Adresse IPv6 de liaison locale. . . . .: fe80::7dc0:aaa2:7e1c:1e1f%18(préféré)   
       Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.167(préféré)
       [...]

 #### Nom, adresse MAC et adresse IP de l'interface Ethernet
     
        Pas d'interface Ethernet

#### Affichez votre gateway

    PS C:\WINDOWS\system32> ipcconfig 
    
    ...
    Carte réseau sans fil Wi-Fi :
    Passerelle par défaut. . . . . . . . . : 10.33.3.253
    ...
    
### En graphique (GUI : Graphical User Interface)

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/Info_interface_wifi.png

###  A quoi sert la gateway dans le réseau d'YNOV ?

    A faire communiquer le réseau D'Ynov avec d'autres réseaux.
    
# 2. Modifications des informations

## A. Modification d'adresse IP (part 1)

###  Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
     PS C:\WINDOWS\system32> New-NetIPAddress -InterfaceIndex 18 -IPAddress 10.33.3.200 -PrefixLength 22 -DefaultGateway 10.33.3.253


    IPAddress         : 10.33.3.200
    InterfaceIndex    : 18
    InterfaceAlias    : Wi-Fi
    AddressFamily     : IPv4
    Type              : Unicast
    PrefixLength      : 22
    PrefixOrigin      : Manual
    SuffixOrigin      : Manual
    AddressState      : Tentative
    ValidLifetime     : Infinite ([TimeSpan]::MaxValue)
    PreferredLifetime : Infinite ([TimeSpan]::MaxValue)
    SkipAsSource      : False
    PolicyStore       : ActiveStore

    IPAddress         : 10.33.3.200
    InterfaceIndex    : 18
    
    Il est possible de perdre Internet car si une deuxième machine possède l'IP que je me suis attribuée, il y aura un conflit IP.
    
## B. Table ARP

#### Depuis la ligne de commande, afficher la table ARP

     PS C:\WINDOWS\system32> arp -a

    Interface : 192.168.244.52 --- 0x2
      Adresse Internet      Adresse physique      Type
      192.168.244.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.234.1 --- 0x5
      Adresse Internet      Adresse physique      Type
      192.168.234.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.244.50 --- 0xa
      Adresse Internet      Adresse physique      Type
      192.168.244.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.56.1 --- 0x10
      Adresse Internet      Adresse physique      Type
      192.168.56.255        ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 10.33.3.200 --- 0x12
      Adresse Internet      Adresse physique      Type
      10.33.3.2             14-4f-8a-65-8c-c7     dynamique
      10.33.3.24            ac-12-03-2e-e4-92     dynamique
      10.33.3.253           00-12-00-40-4c-bf     dynamique
      10.33.3.255           ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 10.10.1.1 --- 0x16
      Adresse Internet      Adresse physique      Type
      10.10.1.255           ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
 
#### Identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement
   
    Pour trouvrer cette adresse, il faut repérer dans la table ARP l'adresse IP de notre passerelle. Elle sera associée à une adresse qui est la MAC : 
    
    10.33.3.253           00-12-00-40-4c-bf     dynamique
    
    
#### Envoyez des ping vers des IP du même réseau que vous
    
        PS C:\WINDOWS\system32> ping 10.33.3.219

        Envoi d’une requête 'Ping'  10.33.3.219 avec 32 octets de données :
        Réponse de 10.33.3.219 : octets=32 temps=163 ms TTL=64
        Réponse de 10.33.3.219 : octets=32 temps=8 ms TTL=64
        Réponse de 10.33.3.219 : octets=32 temps=141 ms TTL=64
        
        PS C:\WINDOWS\system32> ping 10.33.3.232
        
        Envoi d’une requête 'Ping'  10.33.3.232 avec 32 octets de données :
        Réponse de 10.33.3.232 : octets=32 temps=210 ms TTL=64
        Réponse de 10.33.3.232 : octets=32 temps=6 ms TTL=64
        
#### Affichez votre table ARP et listez les adresses MAC associées aux adresses IP que vous avez ping

    PS C:\WINDOWS\system32> arp -a

    [...]
      10.33.3.219           a0-78-17-b5-63-bb     dynamique
      10.33.3.232           88-66-5a-4d-a7-f5     dynamique
    [...]
    
## C. NMAP
      
#### Lancez un scan de ping sur le réseau YNOV
    
    C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92>.\nmap.exe -sP 10.33.0.0/22
    
    [...]
    MAC Address: 40:EC:99:C6:DC:C5 (Intel Corporate)
    Nmap scan report for 10.33.0.196
    Host is up (0.64s latency).
    MAC Address: 22:61:EE:93:67:2D (Unknown)
    Nmap scan report for 10.33.0.199
    Host is up (0.11s latency).
    MAC Address: 70:66:55:EC:4C:A3 (AzureWave Technology)
    Nmap scan report for 10.33.0.203
    Host is up (0.023s latency).
    MAC Address: 44:AF:28:13:6D:2A (Intel Corporate)
    Nmap scan report for 10.33.0.205
    Host is up (0.047s latency).
    MAC Address: F8:5E:A0:99:C3:39 (Intel Corporate)
    Nmap scan report for 10.33.0.208
    Host is up (0.037s latency).
    MAC Address: 70:66:55:C5:4E:29 (AzureWave Technology)
    Nmap scan report for 10.33.0.210
    Host is up (0.011s latency).
    MAC Address: 70:66:55:47:54:15 (AzureWave Technology)
    Nmap scan report for 10.33.0.220
    Host is up (0.017s latency).
    MAC Address: 3C:9C:0F:CA:1F:57 (Intel Corporate)
    Nmap scan report for 10.33.0.224
    Host is up (0.080s latency).
    MAC Address: DC:1B:A1:2D:0A:8C (Intel Corporate)
    Nmap scan report for 10.33.0.226
    Host is up (0.0080s latency).
    MAC Address: F8:FF:C2:2C:61:B5 (Apple)
    Nmap scan report for 10.33.0.244
    [...]
    

#### Affichez votre table ARP

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> arp -a

    Interface : 192.168.244.52 --- 0x2
      Adresse Internet      Adresse physique      Type
      192.168.244.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.234.1 --- 0x5
      Adresse Internet      Adresse physique      Type
      192.168.234.254       00-50-56-f0-1c-79     dynamique
      192.168.234.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.244.50 --- 0xa
      Adresse Internet      Adresse physique      Type
      192.168.244.255       ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.56.1 --- 0x10
      Adresse Internet      Adresse physique      Type
      192.168.56.255        ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 10.33.2.167 --- 0x12
      Adresse Internet      Adresse physique      Type
      10.33.0.67            dc-f5-05-db-cc-ab     dynamique
      10.33.0.71            f0-03-8c-35-fe-47     dynamique
      10.33.0.75            62-77-19-80-6e-28     dynamique
      10.33.0.119           38-f9-d3-63-b1-0f     dynamique
      10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
      10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
      10.33.1.238           50-76-af-88-6c-0b     dynamique
      10.33.2.72            8c-8d-28-35-4c-56     dynamique
      10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
      10.33.3.2             14-4f-8a-65-8c-c7     dynamique
      10.33.3.13            26-7b-3f-46-6d-9e     dynamique
      10.33.3.24            ac-12-03-2e-e4-92     dynamique
      10.33.3.35            ec-2e-98-cc-6d-17     dynamique
      10.33.3.73            14-7d-da-55-4a-ef     dynamique
      10.33.3.77            a0-78-17-6b-62-61     dynamique
      10.33.3.253           00-12-00-40-4c-bf     dynamique
      10.33.3.255           ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
      255.255.255.255       ff-ff-ff-ff-ff-ff     statique

    Interface : 10.10.1.1 --- 0x16
      Adresse Internet      Adresse physique      Type
      10.10.1.255           ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
    
    
## D. Modification d'adresse IP (part 2)

#### Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap 
     C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92>New-NetIPAddress -InterfaceIndex 18 -IPAddress 10.33.3.200 -PrefixLength 22 -DefaultGateway 10.33.3.253
    IPAddress         : 10.33.3.200
    InterfaceIndex    : 18
    InterfaceAlias    : Wi-Fi
    AddressFamily     : IPv4
    Type              : Unicast
    PrefixLength      : 22
    PrefixOrigin      : Manual
    SuffixOrigin      : Manual
    AddressState      : Tentative
    ValidLifetime     : Infinite ([TimeSpan]::MaxValue)
    PreferredLifetime : Infinite ([TimeSpan]::MaxValue)
    SkipAsSource      : False
    PolicyStore       : ActiveStore

    IPAddress         : 10.33.3.200
    InterfaceIndex    : 18
    InterfaceAlias    : Wi-Fi
    AddressFamily     : IPv4
    Type              : Unicast
    PrefixLength      : 22
    PrefixOrigin      : Manual
    SuffixOrigin      : Manual
    AddressState      : Invalid
    ValidLifetime     : Infinite ([TimeSpan]::MaxValue)
    PreferredLifetime : Infinite ([TimeSpan]::MaxValue)
    SkipAsSource      : False
    PolicyStore       : PersistentStore

#### Résultat nmap
   
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> .\nmap.exe -sP 10.33.0.0/22
    
    Nmap scan report for 10.33.3.160
    Host is up (0.036s latency).
    MAC Address: A4:83:E7:71:58:57 (Apple)
    Nmap scan report for 10.33.3.168
    Host is up (0.29s latency).
    MAC Address: 12:88:C1:71:B7:A0 (Unknown)
    Nmap scan report for 10.33.3.178
    Host is up (0.51s latency).
    MAC Address: 70:66:55:D9:A3:39 (AzureWave Technology)
    Nmap scan report for 10.33.3.184
    Host is up (0.081s latency).
    MAC Address: C0:E4:34:18:A0:CF (AzureWave Technology)
    Nmap scan report for 10.33.3.185
    Host is up (0.0040s latency).
    MAC Address: 3C:95:09:DC:A1:15 (Liteon Technology)
    Nmap scan report for 10.33.3.187
    Host is up (0.091s latency).
    MAC Address: 96:FD:87:13:4B:EE (Unknown)
    Nmap scan report for 10.33.3.194
    Host is up (0.022s latency).
    MAC Address: 58:96:1D:DB:E5:6B (Intel Corporate)
    Nmap scan report for 10.33.3.201
    MAC Address: 82:74:57:EE:B9:59 (Unknown)
    Nmap scan report for 10.33.3.203
    Host is up (0.0050s latency).
    MAC Address: 70:66:55:CF:4B:3B (AzureWave Technology)
    Nmap scan report for 10.33.3.207
    Host is up (0.029s latency).
    MAC Address: C0:E4:34:1B:FE:A9 (AzureWave Technology)
    Nmap scan report for 10.33.3.210
    Host is up (0.0070s latency).
    MAC Address: D8:3B:BF:1A:7A:5C (Intel Corporate)
    Nmap scan report for 10.33.3.219
    Host is up (0.10s latency).
    MAC Address: A0:78:17:B5:63:BB (Apple)
    Nmap scan report for 10.33.3.220
    Host is up (0.013s latency).
    MAC Address: 40:EC:99:77:AB:82 (Intel Corporate)
    Nmap scan report for 10.33.3.221
    Host is up (0.026s latency).
    MAC Address: 18:1D:EA:00:7E:D1 (Intel Corporate)
    Nmap scan report for 10.33.3.224
    Host is up (0.0080s latency).
    MAC Address: CE:76:55:BF:50:22 (Unknown)
    Nmap scan report for 10.33.3.232
    Host is up (0.0050s latency).
    MAC Address: 88:66:5A:4D:A7:F5 (Apple)
    Nmap scan report for 10.33.3.245
    Host is up (0.14s latency).
    MAC Address: EC:9B:F3:51:3B:1B (Samsung Electro-mechanics(thailand))
    Nmap scan report for 10.33.3.252
    Host is up (0.0020s latency).
    MAC Address: 00:1E:4F:F9:BE:14 (Dell)
    Nmap scan report for 10.33.3.253
    Host is up (0.0040s latency).
    MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
    Nmap scan report for 10.33.3.254
    Host is up (0.0030s latency).
    MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
    Nmap scan report for 10.33.3.200
    Host is up.
    
#### Prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> ipconfig /all
    
    [...]
    Carte réseau sans fil Wi-Fi :

       Suffixe DNS propre à la connexion. . . :
       Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
       Adresse physique . . . . . . . . . . . : 58-96-1D-14-CA-7B
       DHCP activé. . . . . . . . . . . . . . : Non
       Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.200(préféré)
       Masque de sous-réseau. . . . . . . . . : 255.255.252.0
       Passerelle par défaut. . . . . . . . . : 10.33.3.253
    [...]
    
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> ping 8.8.8.8

    Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
    Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=115
    Réponse de 8.8.8.8 : octets=32 temps=26 ms TTL=115
    
    
    
## II. Exploration locale en duo

 !!! Je ne possède pas de carte wifi j'ai suivi les manipulations avec d'autres élèves jusqu'au chat IP !!!


Après avoir désactivé ma carte WiFi, je configure ma carte ethernet pour utiliser le pc de mon camarade comme gateway
    
    Carte réseau sans fil Wi-Fi :

       Statut du média. . . . . . . . . . . . : Média déconnecté
       [...]

    Carte Ethernet Ethernet :

       Adresse physique . . . . . . . . . . . : D4-5D-64-62-19-14
       DHCP activé. . . . . . . . . . . . . . : Non
       Configuration automatique activée. . . : Oui
       Adresse IPv4. . . . . . . . . . . . . .: 192.168.69.1(préféré)
       Masque de sous-réseau. . . . . . . . . : 255.255.255.252
       Passerelle par défaut. . . . . . . . . : 192.168.69.2


Pour vérifier la connexion à internet, un ping 8.8.8.8 puis un traceroute seront effectués :
        
        Commande : ping 8.8.8.8
        Résultat :
        ping 8.8.8.8

    Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
    Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=114
    Réponse de 8.8.8.8 : octets=32 temps=24 ms TTL=114
    Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
    Réponse de 8.8.8.8 : octets=32 temps=68 ms TTL=114

    Statistiques Ping pour 8.8.8.8:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%)


        Commande : traceroute google.com
        Résultat : 
        > tracert google.com

    Détermination de l’itinéraire vers google.com [216.58.214.174]
    avec un maximum de 30 sauts :

      1    <1 ms     *       <1 ms  LAPTOP-DS2IE5NI.mshome.net [192.168.69.2]
      2     *        *        *     Délai d’attente de la demande dépassé.
      3     8 ms     4 ms     5 ms  10.33.3.253
      4     5 ms     6 ms     3 ms  10.33.10.254
      5     6 ms     3 ms     3 ms  reverse.completel.net [92.103.174.137]
      6     9 ms     8 ms     7 ms  92.103.120.182
      [...]


    La machine LAPTOP-DS2IE5NI.mshome.net [192.168.69.2] est atteinte donc le pc de mon camarade me sert bien de gateway vers Internet

## Petit chat privé

Je suis serveur

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau>netcat-1.11> ./nc.exe -l -p 8888
    test
    test
    Je réponds au test !

    ahah trop bien
    :)
  
Je suis client
    
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau>netcat-1.11> .\nc64.exe 192.168.43.154 8888
    Je t'envoi un test
    C'est ok!

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> Get-NetFirewallProfile -PolicyStore ActiveStore


## Firewall

### Firewall activé:

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> Get-NetFirewallProfile -PolicyStore ActiveStore

    Name                            : Domain
    Enabled                         : True
    [...]

    Name                            : Private
    Enabled                         : True
    [...]

    Name                            : Public
    Enabled                         : True
    [...]
    
### Autoriser les ping

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/firewall_ping_rule.png

  Vérifications:

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> ping 192.168.43.154

    Envoi d’une requête 'Ping'  192.168.43.154 avec 32 octets de données :
    Réponse de 192.168.43.154 : octets=32 temps=7 ms TTL=128
    Réponse de 192.168.43.154 : octets=32 temps=4 ms TTL=128
    Réponse de 192.168.43.154 : octets=32 temps=4 ms TTL=128
    Réponse de 192.168.43.154 : octets=32 temps=4 ms TTL=128

    Statistiques Ping pour 192.168.43.154:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
        Minimum = 4ms, Maximum = 7ms, Moyenne = 4ms
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau>

### Autoriser le traffic sur le port qu'utilise nc

https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/firewall_netcat_rule.png

  Vérification du fonctionnement:

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> .\nc64.exe 192.168.43.154 1337
    test
    test ok !

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> .\nc64.exe -p 1337 -l
    test !
    test ok!

  Vérification du bloquage: 
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/firewall_port_blocked.png

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

#### Afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV et trouver la date d'expiration de votre bail DHCP

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> ipconfig.exe /all
    
    [...]
    Carte réseau sans fil Wi-Fi :

       [...]
       Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 19:25:18
       [...]
       Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
       [...]
       
## 2. DNS

#### Trouver l'adresse IP du serveur DNS que connaît votre ordinateur
    
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau\nmap-7.92> ipconfig.exe /all
   
       [...]
       Carte réseau sans fil Wi-Fi :

       Suffixe DNS propre à la connexion. . . : auvence.co
       Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
       [...]
       Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                           10.33.10.148
                                           10.33.10.155
       [...]
       
       
#### Requêtes DNS

##### Google.com

    PS C:\WINDOWS\system32> nslookup google.com
    
    Serveur :   UnKnown
    Address:  10.33.10.2
    
    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:80e::200e
          216.58.214.174
    
##### ynov.com

    PS C:\WINDOWS\system32> nslookup ynov.com
    Serveur :   UnKnown
    Address:  10.33.10.2
        
    Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Address:  92.243.16.143
    
    --------------------------------------------------
    
    Le serveur DNS d'Ynov a pour adresse 10.33.10.2. La résolution DNS de ces 2 domaines n'est pas extraite du serveur d'Ynov, ce qui explique la phrase "Réponse ne faisant pas autorité ".
    
#### Reverse lookup

##### IP 78.74.21.21
    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> nslookup.exe 78.74.21.21
    Serveur :   UnKnown
    Address:  10.33.10.2

    Nom :    host-78-74-21-21.homerun.telia.com
    Address:  78.74.21.21
    
    Interprétation:
    L'adresse Ip associée à l'adresse 78.74.21.21 est le domaine "host-78-74-21-21.homerun.telia.com"
        
##### IP 92.146.54.88

    PS C:\Users\rouss\OneDrive\Bureau\Ecole\B2\Réseau> nslookup.exe 92.146.54.88
    Serveur :   UnKnown
    Address:  10.33.10.2

    Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
    Address:  92.146.54.88
    
    Interprétation:
        L'adresse Ip associée à l'adresse 92.146.54.88 est le domaine "apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr"
    
# IV. Wireshark

### Capture ICMP
  
    Je ping la gateway
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/Ping_gateway.png

    Vérifications:
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/WireS_ping.png

#### Capture TCP

    Je lance mon serveur netcat et un client se connecte à moi pour échanger:
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/Netcat_server.png

    Je détermine le pacquet comportant de la data :
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/WireS_Ncat_packet.png

    Je retrouve le message transmis par Netcat:
  https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/WireS_find_data.png

#### Capture DNS

    Requète DNS:
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/Request_DNS.png

    Vérifications:
https://gitlab.com/davidroussat/b2-reseau/-/blob/main/TP1/Pictures/WireS_DSN_request.png
